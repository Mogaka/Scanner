"use strict";
 
import React, { Component } from 'react';
import Camera from 'react-native-camera';
import { Container, Content } from 'native-base';
import {postBarcode} from './utilities/post'


import {
  AppRegistry,
  Dimensions,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
  Image
} from 'react-native';

 
export default class App extends Component {



      constructor(props) {
          super(props);

          this.state = {
            barcode: '',
            cameraType: 'back',
            text: [],
            torchMode: 'off',
            flashMode: 'on',
            type: '',
            showCamera: true,
            data: ''
          };
        }
 
    renderCamera(){
        if(this.state.showCamera) {
            return (
                <Camera
                    ref="cam"
                    style={styles.container}
                    showViewFinder={true}
                    cameraType={this.state.cameraType}
                    // onBarCodeRead={this._onBarCodeRead}
                    onBarCodeRead={this._onBarCodeRead.bind(this)}>
                    <Text style={styles.camTitle}>If the camera is not scanning. Please remove from view of qr then repeat</Text>
                    
                </Camera>
            );
        } else {
            return (
                <View style={styles.loading}> 
                  
                  <View style={styles.imgCont}>
                    <Image style={styles.img} source={require('./images/logo.png')} />
                  </View>
                  
                  <Text style={styles.loadingTitle2}> please press the button below to scan again</Text>

                  <TouchableHighlight style={styles.button} onPress={this._onPressButton.bind(this)}>
                      <Text style={styles.buttonTittle} >SCAN</Text>
                  </TouchableHighlight>

                </View>
            );
        }
    }
 
    render(){
        return (
            this.renderCamera()
        );
    }
 
    _onBarCodeRead(e) {
        this.setState({showCamera: false});
        alert(e.data);
      //define payload as object
        const payload = {
          username: '58008-00100@postakenya.posta',
          password: 'attonement',
          data: e.data
        }
        console.log(payload)
       //pass object to func
       postBarcode(payload)
    }


    _onPressButton() {
        this.setState({showCamera: true});
    }
    
}
 
var styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "transparent",
    },

    loading: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#E0FFFE'
    },

    loadingTitle: {
      color: 'skyblue',
      fontSize: 30,
      fontWeight: 'bold',
    },

    loadingTitle2: {
      color: '#9999',
      fontWeight: 'bold',
      fontStyle: 'italic'
    },

    button: {
      marginTop: 100,
      backgroundColor: '#2980b9',
      paddingVertical: 25,
      borderRadius: 10,
      borderWidth: 2,
      elevation: 4,
      borderColor: 'white',
      width: 200.,
      alignItems: 'center'
    },

    buttonTittle: {
      color: 'white',
      fontSize: 30,
      fontWeight: 'bold',
    }, 

    camTitle: {
      fontSize: 15,
      width: 250,
      alignItems: 'center',
      paddingTop: 500,
      color: '#E0FFFE',
      fontStyle: 'italic'
    },

    imgCont: {
      justifyContent: 'center',
      alignItems: 'center',
      paddingBottom: 20
    },

    img: {
      // width: 90,
      height: 70,
      resizeMode: 'contain'
    },

    spin: {
      marginBottom: 10
    }
});